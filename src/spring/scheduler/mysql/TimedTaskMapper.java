import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author lnk
 * @Date 2018/3/6
 */
@Mapper
public interface TimedTaskMapper {

    /**
     * 判断是否被锁
     * @param id
     * @return
     */
    @Select("select count(*) from t_timed_task " +
            "where id = #{id, jdbcType=VARCHAR} and timeout >= UNIX_TIMESTAMP(now())")
    int check(String id);

    /**
     * 判断是否存在锁
     * @param id
     * @return
     */
    @Select("select count(*) from t_timed_task where id = #{id, jdbcType=VARCHAR}")
    int hasId(String id);

    /**
     * 修改锁的过期时间，用返回修改数据数判断是否修改成功
     * @param id
     * @param timeout
     * @return
     */
    @Update("update t_timed_task set timeout = UNIX_TIMESTAMP(now()) + #{timeout, jdbcType=BIGINT} " +
            "where id = #{id, jdbcType=VARCHAR} and timeout < UNIX_TIMESTAMP(now())")
    int lock(@Param("id") String id, @Param("timeout") long timeout);

    /**
     * 插入锁数据，用返回插入数据数判断是否加锁成功
     * @param id
     * @param timeout
     * @param value
     * @return
     */
    @Insert("insert into t_timed_task values ( " +
            "#{id, jdbcType=VARCHAR}," +
            "UNIX_TIMESTAMP(now()) + #{timeout, jdbcType=BIGINT}," +
            "#{value, jdbcType=VARCHAR} )")
    int insert(@Param("id") String id, @Param("timeout") long timeout, @Param("value") String value);

    /**
     * 修改锁的过期时间
     * @param id
     * @param timeout
     * @param value
     * @return
     */
    @Update("update t_timed_task set " +
            "timeout = UNIX_TIMESTAMP(now()) + #{timeout, jdbcType=BIGINT}," +
            "value = #{value, jdbcType=VARCHAR}" +
            "where id = #{id, jdbcType=VARCHAR}")
    int update(@Param("id") String id, @Param("timeout") long timeout, @Param("value") String value);

    /**
     * 获取数据库的系统时间
     * @return
     */
    @Select("select UNIX_TIMESTAMP(now())")
    long time();

    /**
     * 更新服务器级别
     * @param id
     * @param value
     * @param timeout
     * @return
     */
    @Update("update t_timed_task set value = #{value, jdbcType=VARCHAR}," +
            "timeout = UNIX_TIMESTAMP(now()) + #{timeout, jdbcType=BIGINT} " +
            "where id = #{id, jdbcType=VARCHAR} " +
            "and (value > #{value, jdbcType=VARCHAR} or timeout < UNIX_TIMESTAMP(now()))")
    int updateLevel(@Param("id") String id, @Param("value") String value, @Param("timeout") long timeout);

    /**
     * 更新服务器级别的有效时间
     * @param id
     * @param value
     * @param timeout
     * @return
     */
    @Update("update t_timed_task set timeout = UNIX_TIMESTAMP(now()) + #{timeout, jdbcType=BIGINT} " +
            "where id = #{id, jdbcType=VARCHAR} and value = #{value, jdbcType=VARCHAR}")
    int updateLevelTime(@Param("id") String id, @Param("value") String value, @Param("timeout") long timeout);

    /**
     * 获取当前最高级别服务器的级别
     * @param id
     * @return
     */
    @Select("select value from t_timed_task where id = #{id, jdbcType=VARCHAR} and timeout > UNIX_TIMESTAMP(now())")
    String getMaxLevel(String id);
}