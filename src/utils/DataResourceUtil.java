import org.apache.commons.lang3.StringUtils;
import sun.util.locale.provider.LocaleProviderAdapter;
import sun.util.locale.provider.ResourceBundleBasedAdapter;
import sun.util.resources.LocaleData;
import sun.util.resources.OpenListResourceBundle;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 获取JDK内置的一些资源数据
 *
 * @Author lnk
 * @Date 2018/7/20
 */
public class DataResourceUtil {
    private static List<Country> countries;
    private static List<Currency> currencies;
    private static List<Language> languages;

    /**
     * 获取所有国家和对应的二字编码和三字编码
     *
     * @return
     */
    public static List<Country> getCountries() {
        if (countries != null) {
            return countries;
        }

        OpenListResourceBundle resource = getLocaleData().getLocaleNames(Locale.CHINA);
        synchronized (Country.class) {
            if (countries != null) {
                return countries;
            }
            Set<String> data = resource.keySet();
            List<String> twoCodes = data.stream()
                    .filter(code -> !StringUtils.isNumeric(code))
                    .filter(code -> StringUtils.isAllUpperCase(code))
                    .collect(Collectors.toList());
            twoCodes.sort(Comparator.naturalOrder());
            countries = twoCodes.stream().map(code -> {
                Locale locale = new Locale("", code);
                String threeCode = null;
                try {
                    threeCode = locale.getISO3Country();
                } catch (Exception e) {
                }
                return new Country(resource.getString(code), code, threeCode);
            }).collect(Collectors.toList());
            countries = Collections.unmodifiableList(countries);
            return countries;
        }
    }

    /**
     * 获取所有币种和对应的简码
     *
     * @return
     */
    public static List<Currency> getCurrencies() {
        if (currencies != null) {
            return currencies;
        }

        OpenListResourceBundle resource = getLocaleData().getCurrencyNames(Locale.CHINA);
        synchronized (Currency.class) {
            if (currencies != null) {
                return currencies;
            }
            Set<String> data = resource.keySet();
            List<String> codes = data.stream().filter(StringUtils::isAllLowerCase).collect(Collectors.toList());
            codes.sort(Comparator.naturalOrder());
            currencies = codes.stream().map(code -> new Currency(resource.getString(code), code.toUpperCase())).collect(Collectors.toList());
            currencies = Collections.unmodifiableList(currencies);
            return currencies;
        }
    }

    /**
     * 获取系统的语言
     *
     * @return
     */
    public static List<Language> getLanguages() {
        if (languages != null) {
            return languages;
        }

        Locale[] localeList = Locale.getAvailableLocales();
        synchronized (Language.class) {
            if (languages != null) {
                return languages;
            }
            languages = Arrays.stream(localeList).map(l -> {
                String iso3Country = null;
                try {
                    iso3Country = l.getISO3Country();
                } catch (MissingResourceException e) { }
                String language = l.getLanguage();
                String iso3Language = l.getISO3Language();
                String country = l.getCountry();
                String displayCountry = l.getDisplayCountry();
                String displayLanguage = l.getDisplayLanguage();
                String displayName = l.getDisplayName();
                if(StringUtils.isBlank( iso3Country)){
                    return null;
                }
                Country c = new Country(displayCountry, country, iso3Country);
                return new Language(displayLanguage, displayName, language, iso3Language, c);
            }).filter(Objects::nonNull).collect(Collectors.toList());

            languages = Collections.unmodifiableList(languages);
            return languages;
        }
    }

    public static void main(String[] args) {
//        List<Country> countries = getCountries();
//        countries.forEach(System.out::println);
//        List<Currency> currencies = getCurrencies();
//        currencies.forEach(System.out::println);
        List<Language> languages = getLanguages();
        languages.forEach(System.out::println);
    }

    private static LocaleData getLocaleData() {
        ResourceBundleBasedAdapter resource = ((ResourceBundleBasedAdapter) LocaleProviderAdapter.forJRE());
        return resource.getLocaleData();
    }


    public static class Country {
        // 国家名称
        private String name;
        // 二字编码
        private String twoCode;
        // 三字编码
        private String threeCode;

        private Country(String name, String twoCode, String threeCode) {
            this.name = name;
            this.twoCode = twoCode;
            this.threeCode = threeCode;
        }

        public String getName() {
            return name;
        }

        public String getTwoCode() {
            return twoCode;
        }

        public String getThreeCode() {
            return threeCode;
        }

        @Override
        public String toString() {
            return "Country{" +
                    "name='" + name + '\'' +
                    ", twoCode='" + twoCode + '\'' +
                    ", threeCode='" + threeCode + '\'' +
                    '}';
        }
    }

    public static class Currency {
        private String name;
        private String code;

        private Currency(String name, String code) {
            this.name = name;
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public String getCode() {
            return code;
        }

        @Override
        public String toString() {
            return "Currency{" +
                    "name='" + name + '\'' +
                    ", code='" + code + '\'' +
                    '}';
        }
    }

    public static class Language {
        // 语言名称
        private String name;
        // 语言名称使用国家
        private String displayName;
        // 二字编码
        private String twoCode;
        // 三字编码
        private String threeCode;
        // 使用国家
        private Country country;

        public Language(String name, String displayName, String twoCode, String threeCode, Country country) {
            this.name = name;
            this.displayName = displayName;
            this.twoCode = twoCode;
            this.threeCode = threeCode;
            this.country = country;
        }

        public String getName() {
            return name;
        }

        public String getDisplayName() {
            return displayName;
        }

        public String getTwoCode() {
            return twoCode;
        }

        public String getThreeCode() {
            return threeCode;
        }

        public Country getCountry() {
            return country;
        }

        @Override
        public String toString() {
            return "Language{" +
                    "name='" + name + '\'' +
                    ", displayName='" + displayName + '\'' +
                    ", twoCode='" + twoCode + '\'' +
                    ", threeCode='" + threeCode + '\'' +
                    ", country=" + country +
                    '}';
        }
    }
}
