//扫描包下所有类
private static Set<Class<? extends BaseLogBean>> findLogBeans(){
        String packagePath = ClassUtils.convertClassNameToResourcePath(BaseLogBean.class.getPackage().getName());
        packagePath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + packagePath + "/**/*.class";

        try {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
        Resource[] resources = resourcePatternResolver.getResources(packagePath);
        Set<Class<? extends BaseLogBean>> classes = new HashSet<>();
        for (Resource r : resources) {
        MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(r);
        if(metadataReader != null){
        Class clz = Class.forName(metadataReader.getClassMetadata().getClassName());
        if(BaseLogBean.class != clz && BaseLogBean.class.isAssignableFrom(clz)){
        classes.add(clz);
        }
        }
        }
        return classes;
        } catch (IOException e) {
        e.printStackTrace();
        } catch (ClassNotFoundException e) {
        e.printStackTrace();
        }
        return null;
        }

//获取HttpServletRequest对象
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();


/**
 * 根据请求获取网站域名，如果端口是80则省略掉，如：
 * https://www.baidu.com
 * https://www.baidu.com:8080
 * http://127.0.0.1:8080/contextPath
 *
 * @return
 */
public static String getServiceUrl(){
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
        String url = request.getScheme() + "://" + request.getServerName();
        if(request.getServerPort() != 80){
        url += ":" + request.getServerPort();
        }
        url += request.getContextPath();
        return url;
        }



/**
 * 获取SpringMVC所有的请求url
 * @Author lnk
 * @Date 2018/8/14
 */
public class AllRequestMapping implements ApplicationContextAware {
    private ApplicationContext applicationContext;

    public Set<String> getAllRequestMapping(){
        RequestMappingHandlerMapping requestMappingHandlerMapping = applicationContext
                .getBean(RequestMappingHandlerMapping.class);
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping.getHandlerMethods();

        Set<String> allMapping = new HashSet<>();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> entry : handlerMethods.entrySet()) {
            RequestMappingInfo key = entry.getKey();
            HandlerMethod value = entry.getValue();
            PatternsRequestCondition patternsCondition = key.getPatternsCondition();
            Set<String> patterns = patternsCondition.getPatterns();
            allMapping.addAll(patterns);
        }

        return allMapping;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

/**
 * 多个配置文件的时候，查看当前最终生效的是哪些配置
 */
@Configuration
public class ApplicationConfig {

    @Autowired
    private AbstractEnvironment environment;

    /**
     * 打印spring当前生效的配置，主要是用来看在使用多个配置文件的时候，最终生效的是哪些配置
     */
    @PostConstruct
    public void printConfig() {
        LinkedHashSet<String> keys = new LinkedHashSet<>();
        for (PropertySource propertySources : environment.getPropertySources()) {
            if (propertySources instanceof OriginTrackedMapPropertySource) {
                Map<String, Object> source = ((OriginTrackedMapPropertySource) propertySources).getSource();
                keys.addAll(source.keySet());
            }
        }

        List<String> list = new ArrayList<>(keys);
        Collections.sort(list);

        for (String key : list) {
            System.out.println(key + "=" + environment.getProperty(key));
        }

    }
}

/** ---  在Spring应用里手动注入对象的 @Autowired 属性  --- **/
// 如果是Spring Web或是Spring MVC可以直接使用工具类
SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(target);

// 其它Spring应用
@Configuration
public class ApplicationContextHelper implements ApplicationContextAware {
    public static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        ApplicationContextHelper.applicationContext = applicationContext;
    }

    /**
     * 注入对象里面的 @Autowired 属性
     * @param target
     */
    public static void inject(Object target) {
        Assert.notNull(target, "Target object must not be null");

        AutowiredAnnotationBeanPostProcessor bpp = new AutowiredAnnotationBeanPostProcessor();
        bpp.setBeanFactory(applicationContext.getAutowireCapableBeanFactory());
        bpp.processInjection(target);
    }
}




    /** ---  手动注册Spring Bean  --- */
//  SpringApplicationContext
    public static void registerBean(Class<?> cls, Object... constructorArgs){
        ConfigurableApplicationContext context = (ConfigurableApplicationContext) applicationContext;
        //Bean的实例工厂
        DefaultListableBeanFactory dbf = (DefaultListableBeanFactory) context.getAutowireCapableBeanFactory();

        //Bean构建  BeanService.class 要创建的Bean的Class对象
        BeanDefinitionBuilder dataSourceBuilder = BeanDefinitionBuilder.genericBeanDefinition(cls);
        dataSourceBuilder.setScope(BeanDefinition.SCOPE_PROTOTYPE);
        if (constructorArgs != null) {
            for (Object constructorArg : constructorArgs) {
                dataSourceBuilder.addConstructorArgValue(constructorArg);
            }
        }

        // 注册到spring 的bean工厂
        dbf.registerBeanDefinition(beanMame, dataSourceBuilder.getBeanDefinition());
    }

/** ---  手动注册Spring Bean over  --- */



    /** ---  Spring cloud 获取本机ip  --- */
// 注入工具类
@Autowired
private InetUtils inetUtils;
// 获取本地ip
String ip = inetUtils.findFirstNonLoopbackHostInfo().getIpAddress();
